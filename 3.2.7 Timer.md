### 类

#### class machine.Timer(id)	

	id：可以是任意整数，它是定时器的唯一身份认证

#### 定义Timer（定时器）对象
示例：

```py
from machine import Timer

tim1 = Timer(1)
tim2 = Timer(2)
```

### 类函数

#### 1. Timer.deinit()
函数说明：关闭定时器。
<br>示例：

```py
tim1.deinit()
```

#### 2. Timer.init(period, mode, callback)
函数说明：初始化定时器。

	period：定时时长，单位：毫秒。
    	0 < period ≤ 0xCCCC CCCC
    mode：定时模式
    	Timer.ONE_SHOT — 只执行一次
        Timer.PERIODIC — 循环执行
    callback：回调函数
示例：

```py
tim1.init(period=1000, mode=Timer.PERIODIC, callback=lambda t:print("ok"))
```

#### 3. Timer.value()
函数说明：获取并返回计时器当前计数值。
<br>示例：

```py
value = tim1.value()
print(value)
```

### 宏
* Timer.ONE_SHOT &nbsp;&nbsp;=0 &nbsp;&nbsp;&nbsp;— 单次定时
* Timer.PERIODIC &nbsp;&nbsp;=1 &nbsp;&nbsp;&nbsp;— 循环定时

### 综合示例

```py
from machine import Timer
import time

tim1 = Timer(1)
tim1.init(period=1000, mode=Timer.PERIODIC, callback=lambda t:print("ok"))
try:
  while True:
    print(tim1.value())
    time.sleep(1)
except:
  tim1.deinit()
```