### 函数

#### 1. bluetooth.ble_settings(..., adv_man_name, adv_dev_name, ...)

	adv_man_name：厂商名字
	adv_dev_name：设备名字。
    
#### 2. bluetooth.ble_adv_enable(bool)
函数说明： 开始和停止广播。

	bool：
    	True — 开始广播
		False — 停止广播
         
#### 3. bluetooth.init()
函数说明： 启用蓝牙子系统。如果你只调用了deinit()，你只需要调用init()。

#### 4. bluetooth.deinit()
函数说明： 关机蓝牙，由于IDF目前的限制，这不会使BT堆栈返回到较低的状态。

#### 5. bluetooth.connect(bda)
函数说明： GATTC - 连接到远程GATTS服务器。

	BDA：远程地址
    	6个字节，返回一个GATTCConn对象。
        
#### 6. bluetooth.Service(uuid，is_primary = True)
函数说明： GATTS - 创建一个新的GATTSService对象。

	uuid：一个整数或一个字节（16）， 在GATTS中，UUID是全球独一无二的。 如果尝试使用与现有（未关闭）的UUID创建服务，则将收到相同的服务对象，并且不会创建新服务。
    
#### 7. bluetooth.services()
函数说明： GATTS - 返回现有的GATTS服务。

#### 8. bluetooth.conns()
函数说明： GATTC - 返回当前的所有客户端连接。

#### 9. bluetooth.callback(callback，callback_data)
函数说明： 用于设置蓝牙对象级回调的回调函数。

	callback：回调函数
    	可以设置为无
	callback_data：callback调用的参数。
    
#### 10. bluetooth.scan_start()
函数说明： 来查找GATTS设备。您需要设置蓝牙对象回调才能获得扫描结果。

#### 11. bluetooth.scan_stop()
函数说明： GATTC - 提前终止扫描。如果在扫描超时之前调用，您将不会收到一个bluetooth.SCAN_CMPL事件。

#### 12. bluetooth.is_scanning()
函数说明： GATTC - 如果扫描仍然处于活动状态，则返回True。