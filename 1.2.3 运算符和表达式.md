&nbsp;&nbsp;&nbsp;本小节主要说明MicroPython的运算符。举个简单的例子 4 +5 = 9 ， 例子中，4 和 5 被称为操作数，"+" 称为运算符。

### 1.2.3.1 算术运算符



|<div align="center"> 运算符 | <div align="center">名称  |   <div align="center">说明 | <div align="center">  例子 |
|          ---             |               ---           |            ---           |            ---           |
| <div align="center">+        |  <div align="center">  加    |   两个对象相加 |   3 + 5得到8。'a' + 'b'得到'ab'。|
|  <div align="center">  -       |  <div align="center">   减    |定义一个负数，或两个对象相减    |-5.2，或50-24得到26。  |
|  <div align="center">&lowast;       |<div align="center"> 乘   | 两数相乘或返回一个被重复若干次的字符串、列表、元组等   |   2&lowast;3得到6。'la'&lowast;3得到'lalala'。 |
| <div align="center"> /      |  <div align="center">除  |  x除以y  |  4 / 3得到1.333333。6.0/2.0得到3.0。  |
|  <div align="center">//       |  <div align="center"> 取整除 |  返回商的整数部分  |  4 // 3得到1。  |
| <div align="center">%        |  <div align="center">取模  | 返回除法的余数   |  8 % 3得到2。-25.5 % 2.25得到1.5。  |
| <div align="center">&lowast;&lowast;     |  <div align="center"> 幂    |   返回x的y次幂 | 2&lowast;&lowast;3得到8（即 2&lowast;2&lowast;2）。   |

<br>示例：

```py
>>> print(3 + 5, 'a' + 'b') # x + y
8 ab
>>> print(-5.2) # - x
-5.2
>>> print(50 - 24) # x - y
26
>>> print(2 * 3, 'la' * 3)  # x * y
6 lalala
>>> print(4 / 3, 6.0 / 2.0) # x / y
1.333333 3.0
>>> print(4 // 3) # x // y
1
>>> print(8 % 3, -25.5 % 2.25) # x % 3
2 1.5
>>> print(2 ** 3) # x ** y
8
```

### 1.2.3.2 位运算符
&nbsp;&nbsp;&nbsp;位运算符是把数字看作二进制来进行计算的，如5的二进制位为0101。<br>


| <div align="center">运算符 |<div align="center"> 名称  |  <div align="center"> 说明 |  <div align="center"> 例子 |
|     ---   | --- | --- | --- |
| <div align="center"><<      | <div align="center"> 左移    | 把<<左边的运算数的各二进制位全部左移若干位（由<<右边的数指定移动的位数），高位丢弃，低位补0   | 2 << 2得到8（即0010左移两位，右边补0，结果为1000）   |
|  <div align="center"> >>    |  <div align="center"> 右移  | 把>>左边的运算数的各二进制位全部右移若干位，（由>>右边的数指定移动的位数 ），低位丢弃，高位补0| 11 >> 1得到5（即1011右移一位，左边补0，结果为0101）   |
|<div align="center">  &       |   <div align="center">   按位与        |参与运算的两个值，如果相应的两个位都为1，则对应位按位与运算的结果为1，否则为0    | 5 & 3得到1（即0101 & 0011，结果为0001）   |
|<div align="center"> ︱        |     <div align="center">  按位或    |   两个数对应的二进制位有一个为1时，则对应位按位或运算的结果为1，否则为0  |   5︱ 3得到7（即0101︱0011，结果为0111） |
|  <div align="center"> ^      |   <div align="center"> 按位异或    |  两个数对应的二进制位不相同时，则该对应位按位异或运算的结果为1，否则为0  |  5 ^ 3得到6（即0101^0011，结果为0110）  |
| <div align="center">  ~      |  <div align="center">  按位取反    |  每个二进制位取反，即把1变为0，把0变为1。~x 类似于 -x-1  |  ~5得到-6（0101， 在一个有符号二进制数的补码形式。）  |

<br>示例：

```py
>>> print(2 << 2) # x << y
8
>>> print(11 >> 1) # x >> y
5
>>> print(5 & 3) # x & y
1
>>> print(5  | 3) # x | y
7
>>> print(5 ^ 3) # x ^ y
6
>>> print(~5) # ~x
-6
```

### 1.2.3.3 比较运算符



| <div align="center">运算符 |<div align="center"> 名称  |   <div align="center">说明 |   <div align="center">例子 |
| --- | --- | --- | --- |
| <div align="center"><   |   <div align="center">小于 |  返回x是否小于y。若为真返回True，为假返回False |   5<3返回False，3<5返回True |
|<div align="center"> >   | <div align="center">大于   |  返回x是否大于y   | 5>3返回True  |
| <div align="center"> <=  | <div align="center">小于等于   | 返回x是否小于或等于y   | 3<=4返回True   |
| <div align="center">>=   | <div align="center">大于等于   | 返回x是否大于或等于y   | 4>=6返回False   |
| <div align="center"> ==  | <div align="center">等于   | 比较是否相等   | 'str'=='stR'返回False    |
|<div align="center">  !=  | <div align="center">不等于   | 比较是否不相等   | 'str'!='stR'返回True    |

<br>示例：

```py
>>> print(5<3) # x < y
False
>>> print( 5 > 3) # x > y
True
>>> print( 3 <= 4) # x <= y
True
>>> print(4 >= 6) #x >= y
False
>>> print('str' == 'stR') # x== y
False
>>> print('str' != 'stR') # x != y
True
```

### 1.2.3.4 逻辑运算符



| <div align="center">运算符 | <div align="center">名称  |   <div align="center">说明 | <div align="center">  例子 |
| --- | --- | --- | --- |
| <div align="center"> not    | 布尔“非”    |  not x；如果x为True，返回False，否则返回True  |   x=True；not x返回False |
| <div align="center">and   | 布尔“与”    | x and y；x，y都为True返回True，否则返回False  |  x=False；y=True；x and y；由于x是False，返回False。  |
| <div align="center"> or      |  布尔“或”   |  x or y；x或y至少一个为True，返回True，否则返回False |  x=True；y=False；x or y返回True。  |

<br>示例：

```py
>>> print(5 < 3) # not x
False
>>> x = (5 < 3)
>>> print(not x)
True
>>> x = False #  x and y
>>> y = True
>>> print(x and y)
False
>>> x = True # x or y
>>> y = False 
>>> print(x or y)
True
```

#### 1.2.3.5 赋值运算符
&nbsp;&nbsp;&nbsp;算术运算符和简单的赋值运算符“=”结合可构成复杂的赋值运算符。<br>	


|  <div align="center">运算符  |<div align="center"> 描述   |  <div align="center">例子  |
| --- | --- | --- |
| <div align="center">  = | <div align="center"> 简单的赋值运算符  |<div align="center">  c = 10 将10赋值给c|
| <div align="center">+=   |<div align="center">加法赋值运算符    | <div align="center">c += a 等效于 c = c + a   |
|<div align="center"> -=   | <div align="center"> 减法赋值运算符  | <div align="center">  c -= a 等效于 c = c - a |
| <div align="center">&lowast;=   | <div align="center">乘法赋值运算符   | <div align="center"> c &lowast;= a 等效于 c = c &lowast; a  |
| <div align="center"> /=  | <div align="center"> 除法赋值运算符  |  <div align="center">c /= a 等效于 c = c / a  |
| <div align="center">%=   | <div align="center">  取模赋值运算符 | <div align="center"> c %= a 等效于 c = c % a  |
| <div align="center">  &lowast;&lowast;= |<div align="center">  幂赋值运算符  | <div align="center">  c&lowast;&lowast;= a 等效于 c = c &lowast;&lowast; a |
|<div align="center">//=|<div align="center">取整除赋值运算符 | <div align="center">c //= a 等效于 c = c//a|

<br>示例：

```py
>>> c = 10    # = 
>>> print(c)
10
>>> c += 2    # +=
>>> print(c)
12
>>> c -= 2   # -=
>>> print(c)
10
>>> c *= 2   # *=
>>> print(c)
20
>>> c /= 2   # /=
>>> print(c)
10.0
>>> c %= 3   # %=
>>> print(c)
1
>>> c = 10 
>>> c **= 2   # **=
>>> print(c)
100
>>> c //= 10   # //=
>>> print(c)
10
```

**注意：**
<br>&nbsp;&nbsp;&nbsp;赋值运算符是一个整体，中间不能有空格，否则出错。
<br>示例：

```py
>>> c = 10
>>> print(c)
10
>>> c - = 2
Traceback (most recent call last):
  File "<stdin>", line 1
SyntaxError: invalid syntax
```