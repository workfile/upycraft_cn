### 1.2.7.1 类
类定义格式如下：

```
class <类名>:
	类体
```
&nbsp;&nbsp;&nbsp;类中除了定义函数，还可以包含其他语句。一个类中的函数定义通常有一个特殊的参数列表形式，以在类外调用这个函数。

### 1.2.7.2 类对象
&nbsp;&nbsp;&nbsp;类支持两种操作：属性引用和实例化。

#### 属性引用
&nbsp;&nbsp;&nbsp;调用类的属性：obj.name，name是类中定义的变量或函数的名字。
<br>示例：

```py
class MyClass:
  i=12345
  
  def f(self):
    print("hello world")
```
&nbsp;&nbsp;&nbsp;上面示例中MyClass.i和MyClass.f是有效的属性引用，分别引用一个整数和一个函数。

#### 类实例化
示例：

```py
x = MyClass()
```
&nbsp;&nbsp;&nbsp;上面的示例，创建该类的新实例并将对象分配给本地变量x。<br>
&nbsp;&nbsp;&nbsp;一个类可以定义一个名为__init__()的特殊方法。
<br>示例：

```py
def __init__(self， data):
  self.data=23
```
&nbsp;&nbsp;&nbsp;当一个类定义了一个__init__()方法时，类会自动调用__init__()新创建的类实例。可以通过`x=MyClass()`的方式获得。在这种情况下，赋予类实例化的参数被传递给__init__()。
<br>示例：

```py
>>> class ComplexClass:
...   def __inis__(self, r, i):
...     self.r = r
...     self.i = i
...     print(self.r + self.i)
...     
...     
... 
>>> x = ComplexClass(3, 6)
9
>>> x.r, x.i
(3, 6)
```

### 实例对象
示例：

```py
class Complex:
  def __init__(self, r, i):
    self.r = r
    self.i = i
    print(self.r + self.i)
  
x = Complex(3, 6)
x.counter = 1
while x.counter < 10:
    print("hello",x.counter)
    x.counter = x.counter * 2
print(x.counter)
del x.counter
```
运行结果：

```py
9
hello 1
hello 2
hello 4
hello 8
16
```
