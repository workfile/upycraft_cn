## 播放音乐

### 准备
硬件：
* FireBeetle-ESP32 × 1 
* 扬声器 × 1

软件：
* uPyCraft IDE

代码位置：
* <p>File → Examples → Basic → <b>mario.py</b></p>

### 实验步骤
<p>1. 硬件连接：将扬声器的一根线接IO25/D2引脚，一根线接GND引脚。</p>
<p>2. 下载运行 mario.py，其具体代码如下</p>

```py
#硬件平台：FireBeetle-ESP32
#实验效果：成功下载程序后，你可以听到超级玛丽的背景音乐。
#硬件连接：本实验需要在IO25外接一个扬声器模块。
#下面的信息显示，对于当前版本，mario是可用的。
# IO0  IO2  IO4  IO5  IO10 IO12~19 IO21~23 IO25~27

from machine import DAC, Pin, PWM
import math
import time

#创建音乐旋律列表
melody = [
330, 330, 330, 262, 330, 392, 196, 262, 196, 165, 220, 247, 233, 220, 196, 330, 392, 
440, 349, 392, 330, 262, 294, 247, 262, 196, 165, 220, 247, 233, 220, 196, 330, 392,
440, 349, 392, 330, 262, 294, 247, 392, 370, 330, 311, 330, 208, 220, 262, 220, 262,
294, 392, 370, 330, 311, 330, 523, 523, 523, 392, 370, 330, 311, 330, 208, 220, 262,
220, 262, 294, 311, 294, 262, 262, 262, 262, 262, 294, 330, 262, 220, 196, 262, 262,
262, 262, 294, 330, 262, 262, 262, 262, 294, 330, 262, 220, 196]

#创建音调持续时间列表
noteDurations = [
8,4,4,8,4,2,2,
3,3,3,4,4,8,4,8,8,8,4,8,4,3,8,8,3,
3,3,3,4,4,8,4,8,8,8,4,8,4,3,8,8,2,
8,8,8,4,4,8,8,4,8,8,3,8,8,8,4,4,4,8,2,
8,8,8,4,4,8,8,4,8,8,3,3,3,1,
8,4,4,8,4,8,4,8,2,8,4,4,8,4,1,
8,4,4,8,4,8,4,8,2
]

music=PWM(Pin(25))                     
music.duty(512)

for i in range(len(melody)):              #计算melody列表元素的数量并执行循环
  noteDuration = 800/noteDurations[i]
  music.freq(melody[i]*2)
  time.sleep_ms(int(noteDuration * 1.30)) #为每个音调设定时间
  
music.deinit()
```

### 实验效果
&nbsp;&nbsp;&nbsp;成功下载程序后，你可以听到超级玛丽的经典音乐。