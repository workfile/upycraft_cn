## 通过MQTT服务器点亮led
&nbsp;&nbsp;&nbsp;MQTT（Message Queuing Telemetry Transport，消息队列遥测传输），是IBM推出的一种针对移动终端设备的基于TCP/IP的发布/预订协议，可以连接大量的远程传感器和控制设备。

### 准备
硬件：
* FireBeetle-ESP32 × 1
 
软件：
* uPyCraft IDE
* IOT网页端：<a href="http://iot.dfrobot.com.cn/" title="iot.dfrobot.com.cn" target="_blank">http://iot.dfrobot.com.cn</a>
     
代码位置：
* <p>File → Examples → Communicate → <b>mqtt.py</b></p>
* 引用模块：uPy_lib → <b>simple.py</b>

### 实验步骤
**搭建mqtt服务器**
<br><p>1. 登录DFRobot IOT网页端，点击网页右上角的注册/登录。（提示：网页默认英文显示，在注册/登录后面可以切换中文或英文）</p>
<img src=images/5.4.png>

如果你已经注册，直接登录即可。
<p>2. 点击如下图所示的地方，查看你的客户端的用户名、密码以及Client-ID。（客户端辨识码）</p>

<img src=images/6.1.1.png>

<p>3. 点击“添加新的设备”可查看到新设备的TOPIC，如下图所示</p>

<img src=images/6.2.png>

**远程点亮LED**

<p>4.  打开uPyCraft IDE，在device目录下新建umqtt目录，将 simple.py 文件拖动到新建的umqtt目录下，如下图</p>
<img src=images/5.8.1.png>

<p>5. 打开 mqtt.py 文件，按下图所示修改其内容</p>

<img src=images/6.3.10.png>

<p>6. 下载运行修改完成后的mqtt.py文件，可见已经连接到mqtt服务器上，并且订阅了topic：S1daGmJif，如下图</p>

<img src=images/6.4.png>

<p>7. 点击“查看详情”，如下图</p>

<img src=images/6.5.png>

<p>8. 在弹出的新页面中进行如下图所示操作</p>
<img src=images/5.12.png>

### 实验效果
发送命令“on”：

<img src="images/5.20.3.jpg">
 
发送命令“off”：

<img src="images/5.21.3.jpg">

&nbsp;&nbsp;&nbsp;至此，你即学会了远程对LED进行点亮或熄灭操作了。