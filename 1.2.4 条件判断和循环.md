### 1.2.4.1 if语句
&nbsp;&nbsp;&nbsp;if语句用来判断当某个条件成立（非0或为True）时，执行下一个语句。常与else一起使用，表示除if判断条件之外的其他情况。
<br>示例：

```py
>>> num = 130
>>> if num%2 == 0:
...   print(num, "is a even number")
... else:
...   print(num, "is a odd number")
... 
130 is a even number
```

**注意**：
<br>&nbsp;&nbsp;&nbsp;可以有多个elif，else是可选的。elif是“else if”的缩写，对于避免过多的缩进非常有用，else与它最近的前一个if或elif匹配。
<br>示例：

```py
>>> x = 32
>>> if x < 0:
...   print("Negative changed to zero")
... elif x == 0:
...   print("Zero")
... elif x == 1:
...   print("Single")
... else:
...   print("More")
... 
More
```

**注意：**
<br>&nbsp;&nbsp;&nbsp;由于MicroPython严格的缩进格式，为避免出错，最好用空格键进行缩进。
<br>示例：

```py
>>> x = 32
>>> if x > 0:
...   print("x > 0")
... print("hello")
... else:
...   print("x <= 0")
... 
Traceback (most recent call last):
  File "<stdin>", line 3
SyntaxError: invalid syntax
```

### 1.2.4.2 while语句
&nbsp;&nbsp;&nbsp;while语句用于循环执行程序，即在某条件下，循环执行某段程序。
<br>示例：

```py
>>> i = 5
>>> while i > 0:
...   print(i)
...   i = i-1
... 
5
4
3
2
1
```

### 1.2.4.3 for语句
&nbsp;&nbsp;&nbsp;for语句用于循环执行程序，并按序列中的项目（一个列表或一个字符串）顺序迭代。
<br>示例：

```py
>>> words = ['www', 'DFRobot', 'com', 'cn']
>>> for w in words:
...   print(w, len(w))
... 
www 3
DFRobot 7
com 3
cn 2
```
&nbsp;&nbsp;&nbsp;如果需要在for循环内修改迭代的顺序或条件，可以在for循环中增加条件判断。
<br>示例：

```py
>>> words = ['www', 'DFRobot', 'com', 'cn']
>>> for w in words:
...   if len(w) < 7:
...     print(w)
...     
...     
... 
www
com
cn
```

#### range()函数
&nbsp;&nbsp;&nbsp;如果你需要遍历一系列的数字，可以使用内置函数range()。
<br>示例：

```py
>>> for i in range(4):
...     print(i)
...     
...     
... 
0
1
2
3
```

### 1.2.4.4 break语句
&nbsp;&nbsp;&nbsp;break语句用于退出for或while循环。
<br>示例：

```py
>>> for x in range(2, 10):
...   if x == 5:
...     break
...   print(x)
... 
2
3
4
```

### 1.2.4.5 continue语句
&nbsp;&nbsp;&nbsp;continue语句用于退出for或while语句的当前循环，进入下一次循环。
<br>示例：

```py
>>> for x in range(2, 10): 
...   if x == 5:
...     continue
...   print(x)
... 
2
3
4
6
7
8
9
```

### 1.2.4.6 pass语句
&nbsp;&nbsp;&nbsp;pass语句表示空语句，不做任何事情，一般用作占位语句，用来保持程序结构的完整性。
<br>示例：

```py
>>> for letter in 'hello':
...   if letter == 'l':
...     pass
...     print("This is pass")
...   print("Current letter:", letter)
... 
Current letter: h
Current letter: e
This is pass
Current letter: l
This is pass
Current letter: l
Current letter: o
```