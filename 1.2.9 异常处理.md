&nbsp;&nbsp;&nbsp;程序在运行时出错会被中断执行，终端打印出错误信息，这样的问题大家在编写代码的过程中经常遇到。引发程序出错的事件源称为异常，处理程序异常并使程序继续执行的方法叫做异常处理。

### 1.2.9.1 异常
&nbsp;&nbsp;&nbsp;即使语句或表达式在语法上是正确的，但是执行它时却出现错误，我们把这种在执行过程中检测到的错误称为异常。
<br>示例：

```py
c = 10/0
print(c)
```
运行结果：

```py
syntax finish.
>>> 
>>> 
Ready to download this file,please wait!
download ok
exec(open('test.py').read(),globals())
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<string>", line 1, in <module>
ZeroDivisionError: division by zero
```
&nbsp;&nbsp;&nbsp;上面的例子语法没有错误，但是引发ZeroDivisionError异常而产生中断，使后面的程序不能正确执行。

### 1.2.9.2 处理异常

#### **try……except**
&nbsp;&nbsp;&nbsp;在MicroPython中用try……except语句来处理异常，将可能引发异常的语句放到try中执行，当异常发生时，跳过try中剩余的语句，直接跳转至except中的语句来处理异常。
<br>示例：

```py
>>> try:
...   a = 10/0
...   print(a)
... except:
...   print("error")
... 
error
```
&nbsp;&nbsp;&nbsp;except语句也可以专门处理指定的异常，即在except语句后跟异常名称，如果不指定异常名称则表示处理所有异常。
<br>示例：

```py
def divide(x, y):
  try:
    i = x/y
  except ZeroDivisionError:
    print("division by zero!")
  else:
    print("result is", i) 
    
divide(5, 0)
divide(5, 2)
```
运行结果：

```py
division by zero!
2.5
```
&nbsp;&nbsp;&nbsp;在上面的示例中，try……except语句有一个可选的else子句。如果try子句不引发异常，则必须执行该代码。

#### **try……finally**
&nbsp;&nbsp;&nbsp;无论是否发生异常都会执行finally中的语句块，它可以和try……except……else一起使用。
<br>示例：

```py
def divide(x, y):
  try:
    i = x/y
  except ZeroDivisionError:
    print("division by zero!")
  else:
    print("result is", i)
  finally:
    print("executing finally clause")
    
divide(5,0)
print() #打印一个空行
divide(5, 2)
```
运行结果：

```py
division by zero!
executing finally clause

result is 2.5
executing finally clause
```